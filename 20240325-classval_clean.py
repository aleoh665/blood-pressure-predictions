#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Mar 12 14:52:58 2024

@author: alexanderohlson
"""

import pandas as pd
import matplotlib.pyplot as plt
import seaborn as sns
from sklearn.linear_model import LinearRegression
from sklearn.metrics import r2_score
from sklearn.linear_model import Lasso
from sklearn.metrics import mean_squared_error
from sklearn.model_selection import GridSearchCV
from sklearn.linear_model import Ridge
from sklearn.model_selection import RandomizedSearchCV
from scipy.stats import uniform
from sklearn.linear_model import ElasticNet
from sklearn.metrics import confusion_matrix
from sklearn.metrics import classification_report
from sklearn.metrics import roc_auc_score, f1_score
from sklearn.pipeline import Pipeline
from sklearn.preprocessing import StandardScaler
import numpy as np
from sklearn.tree import DecisionTreeRegressor
from scipy.stats import spearmanr

## Load the data sets
# Labels is the minmax values for the samoles
# Set contains the methylation data
main_dir = '/Volumes/KODEN/2024/Python/blood_pressure_startup/data/'
train_labels = pd.read_csv(main_dir + 'ENStrain_labels.csv')
train_set = pd.read_csv(main_dir + 'ENStrain_set.csv')
test_set = pd.read_csv(main_dir + 'ENStest_set.csv')
test_labels = pd.read_csv(main_dir + 'ENStest_lables.csv')

###data preprocessing
X = train_labels.iloc[
    :,
    :,
].values
Y = train_set.iloc[
    :,
    :,
].values


# Define the desired min and max values for systolic
Smin_value = 70
Smax_value = 250

sns.heatmap(train_set.corr())

tresh = (140 - Smin_value) / (Smax_value - Smin_value)


def categorize_values(numbers, tresh):
    above_threshold = []
    below_threshold = []

    for num in numbers:
        if num > tresh:
            above_threshold.append(num)
        else:
            below_threshold.append(num)

    return above_threshold, below_threshold


# Categorize the values
above, below = categorize_values(test_labels['minmax.blood.pressure.systolic'], tresh)

# Create a DataFrame with labels test
fd = pd.DataFrame(
    {
        'Category': [
            '0' if num > tresh else '1'
            for num in train_labels['minmax.blood.pressure.systolic']
        ]
    }
)

# Output the DataFrame
print(fd)


# Create a DataFrame with labels test
df = pd.DataFrame(
    {
        'Category': [
            '0' if num > tresh else '1'
            for num in test_labels['minmax.blood.pressure.systolic']
        ]
    }
)

# Output the DataFrame
print(df)

# %%Reg analysis

##Fitting multiple Linear regression to the training set
regressor = LinearRegression()
regressor.fit(train_set, train_labels)

# regression coeficient
print(regressor.coef_)

##regression Intercept
print(regressor.intercept_)

##validate on training
y_pred1 = regressor.predict(train_set)
print(y_pred1)

R21 = r2_score(train_labels, y_pred1)
print(R21)

# Plotting the predicted vs actual values
plt.figure(figsize=(8, 6))
plt.scatter(train_labels, y_pred1, color='blue', alpha=0.5)
plt.plot(
    [train_labels.min(), train_labels.max()],
    [train_labels.min(), train_labels.max()],
    'k--',
    lw=2,
)  # Plotting the diagonal line
plt.xlabel('Actual')
plt.ylabel('Predicted')
plt.title('Actual vs Predicted values (Regression analysis) Training')
plt.show()

###MSE
Trmse = mean_squared_error(train_labels, y_pred1)
print('Mean Squared Error:', Trmse)

# Convert the data into factors for future analysis
first_column1 = y_pred1[:, 0]
# Convert the first column to a pandas DataFrame with the specified column title
y_pred1 = pd.DataFrame(first_column1, columns=['minmax.blood.pressure.systolic'])
# Make a correlation analysis
correlation_coefficient1 = np.corrcoef(
    y_pred1['minmax.blood.pressure.systolic'],
    train_labels['minmax.blood.pressure.systolic'],
)
print('Correlation Coefficient:', correlation_coefficient1)


##CM
fd1 = pd.DataFrame(
    {'Category': ['0' if num > tresh else '1' for num in y_pred1.iloc[:, 0]]}
)
cm1 = confusion_matrix(fd, fd1)
print(cm1)

# Plot confusion matrix
plt.figure(figsize=(8, 6))
sns.set(font_scale=1.4)  # Adjust font scale
sns.heatmap(
    cm1,
    annot=True,
    fmt='d',
    cmap='Blues',
    xticklabels=['Normal', 'Hypertention'],
    yticklabels=['Normal', 'Hypertention'],
)
plt.xlabel('Predicted labels')
plt.ylabel('True labels')
plt.title('Confusion Matrix')
plt.show()

##CR
cr1 = classification_report(fd, fd1)
print(cr1)

###more parameters to play with
auc_score1 = roc_auc_score(fd, fd1)
f11 = f1_score(fd['Category'].astype(int), fd1['Category'].astype(int))
print(auc_score1)
print(f11)

###spearman correlation
spearman_corr, p_value = spearmanr(
    y_pred1['minmax.blood.pressure.systolic'],
    train_labels['minmax.blood.pressure.systolic'],
)

# Print the Spearman correlation coefficient
print('Spearman correlation coefficient:', spearman_corr)


##test set
y_pred = regressor.predict(test_set)
print(y_pred)

R22 = r2_score(test_labels, y_pred)
print(R22)

Temse = mean_squared_error(test_labels, y_pred)
print('Mean Squared Error:', Temse)

# Plotting the predicted vs actual values
plt.figure(figsize=(8, 6))
plt.scatter(test_labels, y_pred, color='blue', alpha=0.5)
plt.plot(
    [test_labels.min(), test_labels.max()],
    [test_labels.min(), test_labels.max()],
    'k--',
    lw=2,
)  # Plotting the diagonal line
plt.xlabel('Actual')
plt.ylabel('Predicted')
plt.title('Actual vs Predicted values (Regression analysis) Test')
plt.show()


##CM
df1 = pd.DataFrame({'Category': ['0' if num > tresh else '1' for num in y_pred]})
cm2 = confusion_matrix(df, df1)
print(cm2)

# Convert the data into factors for future analysis
first_column2 = y_pred[:, 0]
# Convert the first column to a pandas DataFrame with the specified column title
y_pred = pd.DataFrame(first_column2, columns=['minmax.blood.pressure.systolic'])

correlation_coefficient2 = np.corrcoef(
    y_pred['minmax.blood.pressure.systolic'],
    test_labels['minmax.blood.pressure.systolic'],
)
print('Correlation Coefficient:', correlation_coefficient2)

# Plot confusion matrix
plt.figure(figsize=(8, 6))
sns.set(font_scale=1.4)  # Adjust font scale
sns.heatmap(
    cm2,
    annot=True,
    fmt='d',
    cmap='Blues',
    xticklabels=['Normal', 'Hypertension'],
    yticklabels=[
        'Normal',
        'Hypertension',
    ],
)
plt.xlabel('Predicted labels')
plt.ylabel('True labels')
plt.title('Confusion Matrix')
plt.show()

##CR
cr2 = classification_report(df, df1)
print(cr2)

###more parameters to play with
auc_score2 = roc_auc_score(df, df1)
f12 = f1_score(df['Category'].astype(int), df1['Category'].astype(int))

print(auc_score2)
print(f12)

import pandas as pd
from scipy.stats import spearmanr

print(y_pred1.head())

###spearman correlation
spearman_corr, p_value = spearmanr(
    y_pred['minmax.blood.pressure.systolic'],
    test_labels['minmax.blood.pressure.systolic'],
)

# Print the Spearman correlation coefficient
print('Spearman correlation coefficient:', spearman_corr)

from joblib import dump

##Save model
# dump(regressor, 'Linear_regression_model.joblib')


# %%lasso
# Create and fit the Lasso regression model with alpha = 0.0001
lasso2 = Lasso(alpha=0.0001)
lasso2.fit(train_set, train_labels)

hypT_pred = lasso2.predict(train_set)
print(hypT_pred)

R25 = r2_score(train_labels, hypT_pred)
print(R25)

# Plotting the predicted vs actual values
plt.figure(figsize=(8, 6))
plt.scatter(train_labels, hypT_pred, color='blue', alpha=0.5)
plt.plot(
    [train_labels.min(), train_labels.max()],
    [train_labels.min(), train_labels.max()],
    'k--',
    lw=2,
)  # Plotting the diagonal line
plt.xlabel('Actual')
plt.ylabel('Predicted')
plt.title('Actual vs Predicted values (Lasso tuner) Training')
plt.show()

hypTmse = mean_squared_error(train_labels, hypT_pred)
print('Mean Squared Error:', hypTmse)

##CM
fd2 = pd.DataFrame({'Category': ['0' if num > tresh else '1' for num in hypT_pred]})
cm3 = confusion_matrix(fd, fd2)
print(cm3)

# Plot confusion matrix
plt.figure(figsize=(8, 6))
sns.set(font_scale=1.4)  # Adjust font scale
sns.heatmap(
    cm3,
    annot=True,
    fmt='d',
    cmap='Blues',
    xticklabels=['Normal', 'Hypertension'],
    yticklabels=['Normal', 'Hypertension'],
)
plt.xlabel('Predicted labels')
plt.ylabel('True labels')
plt.title('Confusion Matrix')
plt.show()

# Convert the  column to a pandas DataFrame with the specified column title
hypT_pred = pd.DataFrame(hypT_pred, columns=['minmax.blood.pressure.systolic'])
# Make a correlation analysis
correlation_coefficient3 = np.corrcoef(
    hypT_pred['minmax.blood.pressure.systolic'],
    train_labels['minmax.blood.pressure.systolic'],
)
print('Correlation Coefficient:', correlation_coefficient3)

##CR
cr3 = classification_report(fd, fd2)
print(cr3)

###more parameters to play with
auc_score3 = roc_auc_score(fd, fd2)
f13 = f1_score(fd['Category'].astype(int), fd2['Category'].astype(int))

print(auc_score3)
print(f13)


from scipy.stats import spearmanr

###spearman correlation
spearman_corr1, p_value1 = spearmanr(
    y_pred1['minmax.blood.pressure.systolic'],
    train_labels['minmax.blood.pressure.systolic'],
)

# Print the Spearman correlation coefficient
print('Spearman correlation coefficient:', spearman_corr1)
print('Spearman p-value:', p_value1)

# Make predictions on the test set using the best model
y_pred_best_random = lasso2.predict(test_set)

# Evaluate the best model
mse_best_random = mean_squared_error(test_labels, y_pred_best_random)
print('Best Model Mean Squared Error (Randomized Search):', mse_best_random)

R250 = r2_score(test_labels, y_pred_best_random)
print(R250)

# Plotting the predicted vs actual values
plt.figure(figsize=(8, 6))
plt.scatter(test_labels, y_pred_best_random, color='blue', alpha=0.5)
plt.plot(
    [test_labels.min(), test_labels.max()],
    [test_labels.min(), test_labels.max()],
    'k--',
    lw=2,
)  # Plotting the diagonal line
plt.xlabel('Actual')
plt.ylabel('Predicted')
plt.title('Actual vs Predicted values (Lasso Regression hyperparameters) Test')
plt.show()

##CM
df2 = pd.DataFrame(
    {'Category': ['0' if num > tresh else '1' for num in y_pred_best_random]}
)
cm4 = confusion_matrix(df, df2)
print(cm4)

# Plot confusion matrix
plt.figure(figsize=(8, 6))
sns.set(font_scale=1.4)  # Adjust font scale
sns.heatmap(
    cm4,
    annot=True,
    fmt='d',
    cmap='Blues',
    xticklabels=['Normal', 'Hypertension'],
    yticklabels=['Normal', 'Hypertension'],
)
plt.xlabel('Predicted labels')
plt.ylabel('True labels')
plt.title('Confusion Matrix')
plt.show()

# Convert the  column to a pandas DataFrame with the specified column title
y_pred_best_random = pd.DataFrame(
    y_pred_best_random, columns=['minmax.blood.pressure.systolic']
)
# Make a correlation analysis
correlation_coefficient4 = np.corrcoef(
    y_pred_best_random,
    test_labels['minmax.blood.pressure.systolic'],
)
print('Correlation Coefficient:', correlation_coefficient4)

##CR
cr4 = classification_report(df, df2)
print(cr4)

##more data
auc_score4 = roc_auc_score(df, df2)
f12 = f1_score(df['Category'].astype(int), df2['Category'].astype(int))
print(auc_score4)
print(f12)

# dump(lasso2, 'Lasso1_linear_regression_model.joblib')

###Lasso
# Define the Lasso model
lasso = Lasso()

# Define a distribution of hyperparameters
param_distributions = {
    'lasso__alpha': uniform(loc=0, scale=2),  # Uniform distribution from 0 to 2
    'lasso__fit_intercept': [True, False],
    'lasso__max_iter': [1000, 2000, 3000],  # Different maximum iterations
    'lasso__warm_start': [True, False],
}

# Create a pipeline with feature scaling
pipeline = Pipeline([('scaler', StandardScaler()), ('lasso', lasso)])

random_search = RandomizedSearchCV(
    pipeline,
    param_distributions,
    n_iter=100,
    cv=5,
    scoring='neg_mean_squared_error',
    random_state=42,
)
random_search.fit(train_set, train_labels)

# Print the best hyperparameters
print('Best Hyperparameters:', random_search.best_params_)

# Get the best model
best_lasso_random = random_search.best_estimator_

###test model on train dataset
hypT_pred = best_lasso_random.predict(train_set)
print(hypT_pred)

hypTmse = mean_squared_error(train_labels, hypT_pred)
print('Mean Squared Error:', hypTmse)

R25 = r2_score(train_labels, hypT_pred)
print(R25)

# Plotting the predicted vs actual values
plt.figure(figsize=(8, 6))
plt.scatter(train_labels, hypT_pred, color='blue', alpha=0.5)
plt.plot(
    [train_labels.min(), train_labels.max()],
    [train_labels.min(), train_labels.max()],
    'k--',
    lw=2,
)  # Plotting the diagonal line
plt.xlabel('Actual')
plt.ylabel('Predicted')
plt.title('Actual vs Predicted values (Lasso tuner) Training')
plt.show()

##CM
fd2 = pd.DataFrame({'Category': ['0' if num > tresh else '1' for num in hypT_pred]})
cm3 = confusion_matrix(fd, fd2)
print(cm3)

# Plot confusion matrix
plt.figure(figsize=(8, 6))
sns.set(font_scale=1.4)  # Adjust font scale
sns.heatmap(
    cm3,
    annot=True,
    fmt='d',
    cmap='Blues',
    xticklabels=['Normal', 'Hypertension'],
    yticklabels=['Normal', 'Hypertension'],
)
plt.xlabel('Predicted labels')
plt.ylabel('True labels')
plt.title('Confusion Matrix')
plt.show()

##CR
cr3 = classification_report(fd, fd2)
print(cr3)

###more parameters to play with
auc_score3 = roc_auc_score(fd, fd2)
f13 = f1_score(fd['Category'].astype(int), fd2['Category'].astype(int))

print(auc_score3)
print(f13)

# Make predictions on the test set using the best model
y_pred_best_random = best_lasso_random.predict(test_set)

# Evaluate the best model
mse_best_random = mean_squared_error(test_labels, y_pred_best_random)
print('Best Model Mean Squared Error (Randomized Search):', mse_best_random)

R250 = r2_score(test_labels, y_pred_best_random)
print(R250)

# Plotting the predicted vs actual values
plt.figure(figsize=(8, 6))
plt.scatter(test_labels, y_pred_best_random, color='blue', alpha=0.5)
plt.plot(
    [test_labels.min(), test_labels.max()],
    [test_labels.min(), test_labels.max()],
    'k--',
    lw=2,
)  # Plotting the diagonal line
plt.xlabel('Actual')
plt.ylabel('Predicted')
plt.title('Actual vs Predicted values (Lasso Regression hyperparameters) Test')
plt.show()

##CM
df2 = pd.DataFrame(
    {'Category': ['0' if num > tresh else '1' for num in y_pred_best_random]}
)
cm4 = confusion_matrix(df, df2)
print(cm4)

# Plot confusion matrix
plt.figure(figsize=(8, 6))
sns.set(font_scale=1.4)  # Adjust font scale
sns.heatmap(
    cm4,
    annot=True,
    fmt='d',
    cmap='Blues',
    xticklabels=['Normal', 'Hypertension'],
    yticklabels=['Normal', 'Hypertension'],
)
plt.xlabel('Predicted labels')
plt.ylabel('True labels')
plt.title('Confusion Matrix')
plt.show()

##CR
cr4 = classification_report(df, df2)
print(cr4)

##more data
auc_score4 = roc_auc_score(df, df2)
f12 = f1_score(df['Category'].astype(int), df2['Category'].astype(int))
print(auc_score4)
print(f12)

from scipy.stats import spearmanr

###spearman correlation
spearman_corr3, p_value3 = spearmanr(
    y_pred_best_random,
    test_labels['minmax.blood.pressure.systolic'],
)

# Print the Spearman correlation coefficient
print('Spearman correlation coefficient:', spearman_corr3)
print('Spearman p-value:', p_value3)


# %% Ridge

# Create a Ridge regression model
best_ridge = Ridge(alpha=150)
best_ridge.fit(train_set, train_labels)

# Make predictions on the train set using the best model
y_pred_train = best_ridge.predict(train_set)
print(y_pred_train)

RTmse = mean_squared_error(train_labels, y_pred_train)
print('Mean Squared Error:', RTmse)

R26 = r2_score(train_labels, y_pred_train)
print(R26)

# Plotting the predicted vs actual values
plt.figure(figsize=(8, 6))
plt.scatter(train_labels, y_pred_train, color='blue', alpha=0.5)
plt.plot(
    [train_labels.min(), train_labels.max()],
    [train_labels.min(), train_labels.max()],
    'k--',
    lw=2,
)  # Plotting the diagonal line
plt.xlabel('Actual')
plt.ylabel('Predicted')
plt.title('Actual vs Predicted values (Ridge) Training')
plt.show()

###CM train
fd3 = pd.DataFrame({'Category': ['0' if num > tresh else '1' for num in y_pred_train]})
cm5 = confusion_matrix(fd, fd3)
print(cm5)

# Plot confusion matrix
plt.figure(figsize=(8, 6))
sns.set(font_scale=1.4)  # Adjust font scale
sns.heatmap(
    cm5,
    annot=True,
    fmt='d',
    cmap='Blues',
    xticklabels=['Normal', 'Hypertension'],
    yticklabels=['Normal', 'Hypertension'],
)
plt.xlabel('Predicted labels')
plt.ylabel('True labels')
plt.title('Confusion Matrix')
plt.show()

# Convert the  column to a pandas DataFrame with the specified column title
y_pred_train = pd.DataFrame(y_pred_train, columns=['minmax.blood.pressure.systolic'])
# Make a correlation analysis
correlation_coefficient5 = np.corrcoef(
    y_pred_train['minmax.blood.pressure.systolic'],
    train_labels['minmax.blood.pressure.systolic'],
)
print('Correlation Coefficient:', correlation_coefficient5)

##CR
cr5 = classification_report(fd, fd3)
print(cr5)

###more parameters to play with
auc_score5 = roc_auc_score(fd, fd3)
f15 = f1_score(fd['Category'].astype(int), fd3['Category'].astype(int))


###spearman correlation
spearman_corr, p_value = spearmanr(
    y_pred_train['minmax.blood.pressure.systolic'],
    train_labels['minmax.blood.pressure.systolic'],
)

# Print the Spearman correlation coefficient
print('Spearman correlation coefficient:', spearman_corr)

# Make predictions on the test set using the best model
y_pred_best = best_ridge.predict(test_set)

# Evaluate the best model
mse_best_ridge = mean_squared_error(test_labels, y_pred_best)
print('Best Model Mean Squared Error:', mse_best_ridge)

R26 = r2_score(test_labels, y_pred_best)
print(R26)

# Plotting the predicted vs actual values
plt.figure(figsize=(8, 6))
plt.scatter(test_labels, y_pred_best, color='blue', alpha=0.5)
plt.plot(
    [test_labels.min(), test_labels.max()],
    [test_labels.min(), test_labels.max()],
    'k--',
    lw=2,
)  # Plotting the diagonal line
plt.xlabel('Actual')
plt.ylabel('Predicted')
plt.title('Actual vs Predicted values (Ridge Regression hyperparameters)')
plt.show()

df3 = pd.DataFrame({'Category': ['0' if num > tresh else '1' for num in y_pred_best]})

##CM
cm6 = confusion_matrix(df, df3)
print(cm6)

# Plot confusion matrix
plt.figure(figsize=(8, 6))
sns.set(font_scale=1.4)  # Adjust font scale
sns.heatmap(
    cm6,
    annot=True,
    fmt='d',
    cmap='Blues',
    xticklabels=['Normal', 'Hypertension'],
    yticklabels=['Normal', 'Hypertension'],
)
plt.xlabel('Predicted labels')
plt.ylabel('True labels')
plt.title('Confusion Matrix')
plt.show()

# Convert the  column to a pandas DataFrame with the specified column title
y_pred_best = pd.DataFrame(y_pred_best, columns=['minmax.blood.pressure.systolic'])
# Make a correlation analysis
correlation_coefficient6 = np.corrcoef(
    y_pred_best['minmax.blood.pressure.systolic'],
    test_labels['minmax.blood.pressure.systolic'],
)
print('Correlation Coefficient:', correlation_coefficient6)

##CR
cr6 = classification_report(df, df3)
print(cr6)

##more data
auc_score4 = roc_auc_score(df, df3)
f14 = f1_score(df['Category'].astype(int), df3['Category'].astype(int))


###spearman correlation
spearman_corr, p_value = spearmanr(
    y_pred_best['minmax.blood.pressure.systolic'],
    test_labels['minmax.blood.pressure.systolic'],
)

# Print the Spearman correlation coefficient
print('Spearman correlation coefficient:', spearman_corr)

##Save model
# dump(best_ridge, 'Ridge_linear_regression_model.joblib')

# %% Elastic net

# Create ElasticNet model with specified parameters
elasticnet = ElasticNet(
    alpha=0.01, l1_ratio=0.05, fit_intercept=False, max_iter=500000, tol=0.001
)

elasticnet.fit(train_set, train_labels)
##pedict on the train set
ELNpred1 = elasticnet.predict(train_set)

# Evaluate it
ELNmse1 = mean_squared_error(train_labels, ELNpred1)
print(ELNmse1)

R290 = r2_score(train_labels, ELNpred1)
print(R290)

# Plotting the predicted vs actual values
plt.figure(figsize=(8, 6))
plt.scatter(train_labels, ELNpred1, color='blue', alpha=0.5)
plt.plot(
    [train_labels.min(), train_labels.max()],
    [train_labels.min(), train_labels.max()],
    'k--',
    lw=2,
)  # Plotting the diagonal line
plt.xlabel('Actual')
plt.ylabel('Predicted')
plt.title('Actual vs Predicted values (Elastic net) Training')
plt.show()

###CM train
fd4 = pd.DataFrame({'Category': ['0' if num > tresh else '1' for num in ELNpred1]})
cm70 = confusion_matrix(fd, fd4)
print(cm70)

# Plot confusion matrix
plt.figure(figsize=(8, 6))
sns.set(font_scale=1.4)  # Adjust font scale
sns.heatmap(
    cm70,
    annot=True,
    fmt='d',
    cmap='Blues',
    xticklabels=['Normal', 'Hypertension'],
    yticklabels=['Normal', 'Hypertension'],
)
plt.xlabel('Predicted labels')
plt.ylabel('True labels')
plt.title('Confusion Matrix')
plt.show()

# Convert the  column to a pandas DataFrame with the specified column title
ELNpred1 = pd.DataFrame(ELNpred1, columns=['minmax.blood.pressure.systolic'])
# Make a correlation analysis
correlation_coefficient7 = np.corrcoef(
    y_pred_train['minmax.blood.pressure.systolic'],
    train_labels['minmax.blood.pressure.systolic'],
)
print('Correlation Coefficient:', correlation_coefficient7)

##CR
cr70 = classification_report(fd, fd4)
print(cr70)

auc_score70 = roc_auc_score(fd, fd4)
f170 = f1_score(fd['Category'].astype(int), fd4['Category'].astype(int))

print(auc_score70)
print(f170)

###spearman correlation
spearman_corr, p_value = spearmanr(
    ELNpred1['minmax.blood.pressure.systolic'],
    train_labels['minmax.blood.pressure.systolic'],
)

# Print the Spearman correlation coefficient
print('Spearman correlation coefficient:', spearman_corr)

# Make predictions on the test set using the best model
ELNpred2 = elasticnet.predict(test_set)

# Evaluate the best model
ELNmse2 = mean_squared_error(test_labels, ELNpred2)
print('Mean Squared Error:', ELNmse2)

R2010 = r2_score(test_labels, ELNpred2)
print(R2010)

# Plotting the predicted vs actual values
plt.figure(figsize=(8, 6))
plt.scatter(test_labels, ELNpred2, color='blue', alpha=0.5)
plt.plot(
    [test_labels.min(), test_labels.max()],
    [test_labels.min(), test_labels.max()],
    'k--',
    lw=2,
)  # Plotting the diagonal line
plt.xlabel('Actual')
plt.ylabel('Predicted')
plt.title('Actual vs Predicted values (Ridge Regression hyperparameters)')
plt.show()

df40 = pd.DataFrame({'Category': ['0' if num > tresh else '1' for num in ELNpred2]})

##CM
cm80 = confusion_matrix(df, df40)
print(cm80)

# Plot confusion matrix
plt.figure(figsize=(8, 6))
sns.set(font_scale=1.4)  # Adjust font scale
sns.heatmap(
    cm80,
    annot=True,
    fmt='d',
    cmap='Blues',
    xticklabels=['Normal', 'Hypertention'],
    yticklabels=['Normal', 'Hypertension'],
)
plt.xlabel('Predicted labels')
plt.ylabel('True labels')
plt.title('Confusion Matrix')
plt.show()

# Convert the  column to a pandas DataFrame with the specified column title
ELNpred2 = pd.DataFrame(ELNpred2, columns=['minmax.blood.pressure.systolic'])
# Make a correlation analysis
correlation_coefficient8 = np.corrcoef(
    ELNpred2['minmax.blood.pressure.systolic'],
    test_labels['minmax.blood.pressure.systolic'],
)
print('Correlation Coefficient:', correlation_coefficient8)

##CR
cr80 = classification_report(df, df40)
print(cr80)

##more data
auc_score80 = roc_auc_score(df, df40)
f140 = f1_score(df['Category'].astype(int), df40['Category'].astype(int))

print(auc_score80)
print(f140)

###spearman correlation
spearman_corr, p_value = spearmanr(
    ELNpred2['minmax.blood.pressure.systolic'],
    test_labels['minmax.blood.pressure.systolic'],
)

# Print the Spearman correlation coefficient
print('Spearman correlation coefficient:', spearman_corr)

##Save model
dump(elasticnet, 'Elasticnet_linear_regression_model.joblib')


# Create the Elastic Net model
elastic_net = ElasticNet()

# Define the grid of hyperparameters
param_grid = {
    'alpha': [0.1, 0.5, 1.0],  # Regularization parameter
    'l1_ratio': [0.2, 0.5, 0.8],  # Ratio between L1 and L2 penalties
    #'fit_intercept': [True, False],  # Whether to fit the intercept
    'max_iter': [10000000],  # Maximum number of iterations
    'tol': [1e-4],  # Tolerance for optimization algorithm
}

# Perform grid search cross-validation
grid_search = GridSearchCV(
    estimator=elastic_net, param_grid=param_grid, cv=5, scoring='neg_mean_squared_error'
)
grid_search.fit(train_set, train_labels)

# Print the best hyperparameters
print('Best Hyperparameters:', grid_search.best_params_)

# Get the best model
best_elastic_net = grid_search.best_estimator_

# Test on the training
ERHy_pred = best_elastic_net.predict(train_set)

# Evaluate the model
ERHmse = mean_squared_error(train_labels, ERHy_pred)
print('Mean Squared Error:', ERHmse)

R29 = r2_score(train_labels, ERHy_pred)
print(R29)

# Plotting the predicted vs actual values
plt.figure(figsize=(8, 6))
plt.scatter(train_labels, ERHy_pred, color='blue', alpha=0.5)
plt.plot(
    [train_labels.min(), train_labels.max()],
    [train_labels.min(), train_labels.max()],
    'k--',
    lw=2,
)  # Plotting the diagonal line
plt.xlabel('Actual')
plt.ylabel('Predicted')
plt.title('Actual vs Predicted values (Elastic net) Training')
plt.show()

###CM train
fd4 = pd.DataFrame({'Category': ['0' if num > tresh else '1' for num in ERHy_pred]})
cm7 = confusion_matrix(fd, fd4)
print(cm7)

# Plot confusion matrix
plt.figure(figsize=(8, 6))
sns.set(font_scale=1.4)  # Adjust font scale
sns.heatmap(
    cm7,
    annot=True,
    fmt='d',
    cmap='Blues',
    xticklabels=['Normal', 'Hypertension'],
    yticklabels=['Normal', 'Hypertension'],
)
plt.xlabel('Predicted labels')
plt.ylabel('True labels')
plt.title('Confusion Matrix')
plt.show()

##CR
cr7 = classification_report(fd, fd4)
print(cr7)

###more parameters to play with
auc_score7 = roc_auc_score(fd, fd4)
f17 = f1_score(fd['Category'].astype(int), fd4['Category'].astype(int))

print(auc_score7)
print(f17)

# Make predictions on the test set using the best model
ELH_predy = best_elastic_net.predict(test_set)

# Evaluate the best model
ELHmse = mean_squared_error(test_labels, ELH_predy)
print('Mean Squared Error:', ELHmse)

R201 = r2_score(test_labels, ELH_predy)
print(R201)

# Plotting the predicted vs actual values
plt.figure(figsize=(8, 6))
plt.scatter(test_labels, ELH_predy, color='blue', alpha=0.5)
plt.plot(
    [test_labels.min(), test_labels.max()],
    [test_labels.min(), test_labels.max()],
    'k--',
    lw=2,
)  # Plotting the diagonal line
plt.xlabel('Actual')
plt.ylabel('Predicted')
plt.title('Actual vs Predicted values (Ridge Regression hyperparameters)')
plt.show()

df4 = pd.DataFrame({'Category': ['0' if num > tresh else '1' for num in ELH_predy]})

##CM
cm8 = confusion_matrix(df, df4)
print(cm8)

# Plot confusion matrix
plt.figure(figsize=(8, 6))
sns.set(font_scale=1.4)  # Adjust font scale
sns.heatmap(
    cm8,
    annot=True,
    fmt='d',
    cmap='Blues',
    xticklabels=['Normal', 'Hypertention'],
    yticklabels=['Normal', 'Hypertension'],
)
plt.xlabel('Predicted labels')
plt.ylabel('True labels')
plt.title('Confusion Matrix')
plt.show()

##CR
cr8 = classification_report(df, df4)
print(cr8)

##more data
auc_score8 = roc_auc_score(df, df4)
f14 = f1_score(df['Category'].astype(int), df4['Category'].astype(int))

print(auc_score8)
print(f14)

# %%Reg hyperparameter analysis
# Define the regressor
regressor = DecisionTreeRegressor()

# Define the hyperparameters to tune
param_grid = {
    'max_depth': [3, 5, 7, 9],
    'min_samples_split': [2, 5, 10],
    'min_samples_leaf': [1, 2, 4],
}

# Initialize GridSearchCV
grid_search = GridSearchCV(
    regressor, param_grid, cv=5, scoring='neg_mean_squared_error', verbose=1
)

# Fit the grid search to the data
grid_search.fit(train_set, train_labels)

# Print the best parameters found
print('Best parameters:', grid_search.best_params_)

# Get the best model
best_regressor = grid_search.best_estimator_

# Test on the training
EHy_pred = best_regressor.predict(train_set)

# Evaluate the model
EHmse = mean_squared_error(train_labels, EHy_pred)
print('Mean Squared Error:', EHmse)

R210 = r2_score(train_labels, EHy_pred)
print(R210)

# Plotting the predicted vs actual values
plt.figure(figsize=(8, 6))
plt.scatter(train_labels, EHy_pred, color='blue', alpha=0.5)
plt.plot(
    [train_labels.min(), train_labels.max()],
    [train_labels.min(), train_labels.max()],
    'k--',
    lw=2,
)  # Plotting the diagonal line
plt.xlabel('Actual')
plt.ylabel('Predicted')
plt.title('Actual vs Predicted values (Elastic net) Training')
plt.show()

###CM train
fd5 = pd.DataFrame({'Category': ['0' if num > tresh else '1' for num in EHy_pred]})
cm9 = confusion_matrix(fd, fd5)
print(cm9)

# Plot confusion matrix
plt.figure(figsize=(8, 6))
sns.set(font_scale=1.4)  # Adjust font scale
sns.heatmap(
    cm9,
    annot=True,
    fmt='d',
    cmap='Blues',
    xticklabels=['Normal', 'Hypertension'],
    yticklabels=['Normal', 'Hypertension'],
)
plt.xlabel('Predicted labels')
plt.ylabel('True labels')
plt.title('Confusion Matrix')
plt.show()

# Convert the  column to a pandas DataFrame with the specified column title
EHy_pred = pd.DataFrame(EHy_pred, columns=['minmax.blood.pressure.systolic'])
# Make a correlation analysis
correlation_coefficient9 = np.corrcoef(
    EHy_pred['minmax.blood.pressure.systolic'],
    train_labels['minmax.blood.pressure.systolic'],
)
print('Correlation Coefficient:', correlation_coefficient9)

##CR
cr9 = classification_report(fd, fd5)
print(cr9)

###more parameters to play with
auc_score9 = roc_auc_score(fd, fd5)
f19 = f1_score(fd['Category'].astype(int), fd5['Category'].astype(int))

print(auc_score9)
print(f19)

# Make predictions on the test set using the best model
EH_predy = best_regressor.predict(test_set)

# Evaluate the best model
EHmse2 = mean_squared_error(test_labels, EH_predy)
print('Mean Squared Error:', EHmse2)

R211 = r2_score(test_labels, EH_predy)
print(R211)

# Plotting the predicted vs actual values
plt.figure(figsize=(8, 6))
plt.scatter(test_labels, EH_predy, color='blue', alpha=0.5)
plt.plot(
    [test_labels.min(), test_labels.max()],
    [test_labels.min(), test_labels.max()],
    'k--',
    lw=2,
)  # Plotting the diagonal line
plt.xlabel('Actual')
plt.ylabel('Predicted')
plt.title('Actual vs Predicted values (Ridge Regression hyperparameters)')
plt.show()

df5 = pd.DataFrame({'Category': ['0' if num > tresh else '1' for num in EH_predy]})

##CM
cm10 = confusion_matrix(df, df5)
print(cm10)

# Plot confusion matrix
plt.figure(figsize=(8, 6))
sns.set(font_scale=1.4)  # Adjust font scale
sns.heatmap(
    cm10,
    annot=True,
    fmt='d',
    cmap='Blues',
    xticklabels=['Normal', 'Hypertention'],
    yticklabels=['Normal', 'Hypertension'],
)
plt.xlabel('Predicted labels')
plt.ylabel('True labels')
plt.title('Confusion Matrix')
plt.show()

# Convert the  column to a pandas DataFrame with the specified column title
EH_predy = pd.DataFrame(EH_predy, columns=['minmax.blood.pressure.systolic'])
# Make a correlation analysis
correlation_coefficient10 = np.corrcoef(
    EH_predy['minmax.blood.pressure.systolic'],
    test_labels['minmax.blood.pressure.systolic'],
)
print('Correlation Coefficient:', correlation_coefficient10)

##CR
cr10 = classification_report(df, df5)
print(cr10)

##more data
auc_score10 = roc_auc_score(df, df5)
f16 = f1_score(df['Category'].astype(int), df5['Category'].astype(int))

print(auc_score10)
print(f16)
