#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue May 14 13:41:08 2024

@author: alexanderohlson
"""

main_dir = '/Volumes/KODEN/2024/Python/blood_pressure_startup/data/'
import pandas as pd
from sklearn.model_selection import train_test_split
import seaborn as sns
import matplotlib.pyplot as plt

# Read CSV file into a pandas DataFrame
df = pd.read_csv(main_dir + 'L3.csv', header=None)
df2 = pd.read_csv(main_dir + 'infodata.csv')

# Define the desired min and max values for systolic
Smin_value = 70
Smax_value = 250

# Apply min-max normalization
BPS2 = (df2['blood.pressure.systolic'] - Smin_value) / (Smax_value - Smin_value)

##rename anc coreect
BPS2 = BPS2.rename('minmax.blood.pressure.systolic')
df2 = df2.drop(columns=['minmax.blood.pressure.systolic'])

# %% data preprocessing

BPS3 = df2[['age', 'systolic', 'sex']]

combined_df = pd.concat([df, BPS3, BPS2], axis=1)
combined_df.index = df2['sample.id']

sns.countplot(combined_df, x='systolic')

###remoove bad samples
combined_df = combined_df[~combined_df['systolic'].isin(['BAD', 'Hypotension'])]
sns.countplot(combined_df, x='systolic')

##categories
categories = ['Normal', 'Hypertension']

# %% Split and get a
# Assuming combined_df is your DataFrame with the dataset

# 1. Separate 'Hypertension' and 'Normal' classes
hypertension_df = combined_df[combined_df['systolic'] == 'Hypertension']
normal_df = combined_df[combined_df['systolic'] == 'Normal']

# 2. Sort 'Hypertension' samples based on 'minmax.blood.pressure.systolic' column
sorted_hypertension_df = hypertension_df.sort_values(
    by='minmax.blood.pressure.systolic', ascending=False
)

# 3. Select the top 50% of 'Hypertension' samples
top_50_percent_hypertension = sorted_hypertension_df.head(
    int(len(sorted_hypertension_df) * 0.5)
)

# 4. Randomly select the same number of samples from 'Normal' class
random_normal_samples = normal_df.sample(
    n=len(top_50_percent_hypertension), random_state=42
)  # You can use any random state you want

# 5. Combine the selected samples from both classes
selected_samples = pd.concat(
    [top_50_percent_hypertension, random_normal_samples], axis=0
)

# 6. Shuffle the combined DataFrame to randomize the order of samples
selected_samples = selected_samples.sample(frac=1, random_state=42).reset_index(
    drop=True
)  # Shuffle and reset index

# %% plotting data

##Pressure in group
# Set the figure size before any plotting
plt.figure(figsize=(10, 10))

# Create a boxplot
sns.boxplot(x='systolic', y='minmax.blood.pressure.systolic', data=selected_samples)

# Rotate x-axis labels for better readability if needed
plt.xticks(rotation=45)

# Set the font scale
sns.set(font_scale=2)

# Ensure the plot is rendered correctly before saving
plt.tight_layout()

# Save the plot as an SVG file
plt.savefig('pressure-group.svg', format='svg')

# Show the plot
plt.show()

##Pressure in sex
# Set the figure size before any plotting
plt.figure(figsize=(10, 10))

# Create a boxplot
sns.boxplot(x='sex', y='minmax.blood.pressure.systolic', data=selected_samples)

# Rotate x-axis labels for better readability if needed
plt.xticks(rotation=45)

# Set the font scale
sns.set(font_scale=2)

# Ensure the plot is rendered correctly before saving
plt.tight_layout()

# Save the plot as an SVG file
plt.savefig('pressure-sex.svg', format='svg')

# Show the plot
plt.show()

##age in group
# Set the figure size before any plotting
plt.figure(figsize=(10, 10))

# Create a boxplot
sns.boxplot(x='systolic', y='age', data=selected_samples)

# Rotate x-axis labels for better readability if needed
plt.xticks(rotation=45)

# Set the font scale
sns.set(font_scale=2)

# Ensure the plot is rendered correctly before saving
plt.tight_layout()

# Save the plot as an SVG file
plt.savefig('age-group.svg', format='svg')

# Show the plot
plt.show()

##sex in group
# Set the figure size before any plotting
plt.figure(figsize=(10, 10))

# Create a countplot
sns.countplot(x='systolic', hue='sex', data=selected_samples)

# Rotate x-axis labels for better readability if needed
plt.xticks(rotation=45)

# Set the font scale
sns.set(font_scale=2)

# Ensure the plot is rendered correctly before saving
plt.tight_layout()

# Save the plot as an SVG file
plt.savefig('sex-group.svg', format='svg')

# Show the plot
plt.show()

# %%

# Separate the dataset into subsets based on categories
category_subsets = {}
for category in categories:
    category_subsets[category] = selected_samples[
        selected_samples['systolic'] == category
    ]

# Determine the minimum number of samples among all categories
min_samples = min(len(category_subsets[category]) for category in categories)

train_set = pd.DataFrame()
test_set = pd.DataFrame()

for category in categories:
    subset = category_subsets[category].sample(n=min_samples, random_state=42)
    train, test = train_test_split(subset, test_size=0.3, random_state=42)
    train_set = pd.concat([train_set, train], ignore_index=True)
    test_set = pd.concat([test_set, test], ignore_index=True)

# Shuffle the final sets to ensure randomness
train_set = train_set.sample(frac=1, random_state=42).reset_index(drop=True)
test_set = test_set.sample(frac=1, random_state=42).reset_index(drop=True)

# Now you have balanced training, validation, and test sets with the same number of samples from each category

sns.countplot(train_set, x='systolic')
sns.countplot(test_set, x='systolic')

###create lables
train_labels = train_set['minmax.blood.pressure.systolic']
test_labels = test_set['minmax.blood.pressure.systolic']

# remoove category
train_set = train_set.drop(
    columns=['minmax.blood.pressure.systolic', 'age', 'systolic', 'sex']
)
test_set = test_set.drop(
    columns=['minmax.blood.pressure.systolic', 'age', 'systolic', 'sex']
)

###export

# train_labels.to_csv(main_dir + 'ENStrain_labels.csv', index=False)
# train_set.to_csv(main_dir + 'ENStrain_set.csv', index=False)
# test_set.to_csv(main_dir + 'ENStest_set.csv', index=False)
# test_lables.to_csv(main_dir + 'ENStest_lables.csv', index=False)
